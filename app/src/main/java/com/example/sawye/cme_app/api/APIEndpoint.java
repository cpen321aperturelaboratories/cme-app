package com.example.sawye.cme_app.api;

import com.example.sawye.cme_app.model.Event;
import com.example.sawye.cme_app.model.CMe;
import com.example.sawye.cme_app.model.Profile;
import com.example.sawye.cme_app.model.User;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by JR on 10/14/2017.
 */

public interface APIEndpoint {


    //----------------------------------------------------------------------------------------------
    // AUTHENTICATION HANDLERS
    //----------------------------------------------------------------------------------------------
    @POST("login/")
    @FormUrlEncoded
    Call<ResponseBody> login(@Field("username") String user,
                             @Field("password") String password);

    @GET("logout/")
    Call<ResponseBody> logout();


    //----------------------------------------------------------------------------------------------
    // USER HANDLERS
    //----------------------------------------------------------------------------------------------
    @Headers("Content-Type:application/json")
    @POST("users/")
    Call<ResponseBody> createUser(@Body List<CMe<User>> user);

    @DELETE("users/{user_id}/")
    Call<ResponseBody> deleteUser(@Path("user_id") Integer user_id);

    @GET("users/{user_id}/")
    Call<ResponseBody> getUser(@Path("user_id") Integer user_id);

    @GET("users/{user_id}/profile/")
    Call<Integer> getUserProfile(@Path("user_id") Integer user_id);

    @Headers("Content-Type:application/json")
    @PUT("users/{user_id}/")
    Call<ResponseBody> updateUser(@Path("user_id") Integer user_id,
                                  @Body List<CMe<User>> user);


    //----------------------------------------------------------------------------------------------
    // EVENT HANDLERS
    //----------------------------------------------------------------------------------------------
    @Headers("Content-Type:application/json")
    @POST("events/")
    Call<ResponseBody> createEvent(@Body List<CMe<Event>> event);

    @DELETE("events/{event_id}/")
    Call<ResponseBody> deleteEvent(@Path("event_id") Integer event_id);

    @Headers("Content-Type:application/json")
    @POST("events/{event_id}/join/")
    Call<ResponseBody> joinEvent(@Path("event_id") Integer event_id);

    @Headers("Content-Type:application/json")
    @POST("events/{event_id}/leave/")
    Call<ResponseBody> leaveEvent(@Path("event_id") Integer event_id);

    @GET("events/{event_id}/")
    Call<ResponseBody> getEventById(@Path("event_id") Integer event_id);

    @Headers("Content-Type:application/json")
    @PUT("events/{event_id}/")
    Call<ResponseBody> updateEvent(@Path("event_id") Integer event_id,
                                   @Body List<CMe<Event>> event);

    @GET("events/search/")
    Call<ResponseBody> getEventsByCategories(@Query("category[]") List<String> category,
                                             @Query("sorting_strategy") String strategy,
                                             @Query("event_index") Integer offset,
                                             @Query("num_events") Integer size);

    @GET("events/search/")
    Call<ResponseBody> getEventsByOwner(@Query("owner_id") Integer owner_id,
                                        @Query("sorting_strategy") String strategy,
                                        @Query("event_index") Integer offset,
                                        @Query("num_events") Integer size);

    @Multipart
    @POST("events/{event_id}/photo/")
    Call<ResponseBody> uploadEventPhoto(@Path("event_id") Integer event_id,
                                        @Part MultipartBody.Part file);

    @GET("events/{event_id}/photo/")
    Call<ResponseBody> getEventPhoto(@Path("event_id") Integer event_id);


    //----------------------------------------------------------------------------------------------
    // PROFILE HANDLERS
    //----------------------------------------------------------------------------------------------
    @GET("profiles/{profile_id}/")
    Call<ResponseBody> getProfile(@Path("profile_id") Integer profile_id);

    @Headers("Content-Type:application/json")
    @PUT("profiles/{profile_id}/")
    Call<ResponseBody> updateProfile(@Path("profile_id") Integer profile_id,
                                     @Body List<CMe<Profile>> profile);

    @Multipart
    @POST("profiles/{profile_id}/photo")
    Call<ResponseBody> uploadProfilePhoto(@Path("profile_id") Integer profile_id,
                                          @Part MultipartBody.Part file);

    @GET("profiles/{profile_id}/photo")
    Call<ResponseBody> getProfilePhoto(@Path("profile_id") Integer profile_id);
}

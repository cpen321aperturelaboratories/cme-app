package com.example.sawye.cme_app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.example.sawye.cme_app.model.CMe;
import com.example.sawye.cme_app.model.Event;
import com.example.sawye.cme_app.remote.HandlerMessageConstants;
import com.example.sawye.cme_app.remote.ServerInterface;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by sawye on 10/16/2017.
 */

public class ViewEventActivity extends AppCompatActivity implements DeleteEventDialogFragment.DeleteEventDialogListener {

    public static final String FINISH_THIS = "FINISH_THIS";
    private ServerInterface mInterface;
    private CMe<Event> event;
    private ImageView eventPhoto;
    public static Toast toast;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.hasExtra(FINISH_THIS)) {
            finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_event);
        ViewFlipper vf = (ViewFlipper) findViewById(R.id.viewFlipper);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        event = (CMe<Event>) bundle.getSerializable("Event");

        // Server interface
        mInterface = ServerInterface.getInstance();

        // If we own this event, allow editing and deleting event
        if (event.getFields().getOwner() == mInterface.userId) {
            vf.setDisplayedChild(vf.indexOfChild(findViewById(R.id.owner)));
        }
        // The event is not in our rsvp list
        else if (State.mProfile.get(0).getFields().getRsvpList() == null) {
            vf.setDisplayedChild(vf.indexOfChild(findViewById(R.id.not_going)));
        }
        else if (!State.mProfile.get(0).getFields().getRsvpList().contains(event.pk)) {
            vf.setDisplayedChild(vf.indexOfChild(findViewById(R.id.not_going)));
        }
        else { // The event is in our rsvp list
            vf.setDisplayedChild(vf.indexOfChild(findViewById(R.id.going)));
        }

        // Set fields for non-owner
        TextView eventName = (TextView) this.findViewById(R.id.event_name_field);
        TextView eventDetails = (TextView) this.findViewById(R.id.event_details_field);
        TextView eventCategory = (TextView) this.findViewById(R.id.event_category_field);
        TextView eventLocation = (TextView) this.findViewById(R.id.event_location_field);
        TextView eventDateStart = (TextView) this.findViewById(R.id.start_date_field);
        TextView eventTimeStart = (TextView) this.findViewById(R.id.start_time_field);
        TextView eventDateEnd = (TextView) this.findViewById(R.id.end_date_field);
        TextView eventTimeEnd = (TextView) this.findViewById(R.id.end_time_field);
        eventPhoto = (ImageView) this.findViewById(R.id.event_picture_field);

        eventName.setText(event.getFields().getName());
        eventDetails.setText(event.getFields().getDescription());
        eventCategory.setText(getCategoryName(event.getFields()));
        eventLocation.setText(event.getFields().getAddress());
        eventDateStart.setText(getDate(event.getFields().getEventStart()));
        eventTimeStart.setText(getTime(event.getFields().getEventStart()));
        eventDateEnd.setText(getDate(event.getFields().getEventEnd()));
        eventTimeEnd.setText(getTime(event.getFields().getEventEnd()));
        setImage(event.getFields());
        eventDetails.setHorizontallyScrolling(false);
        eventDetails.setLines(5);

        MyLocationViewer locationViewer = new MyLocationViewer(this, R.id.event_location_field);

        // Handler
        Handler mHandler = new ViewEventActivity.CommunicationHandler();
        mInterface.setHandler(mHandler);
    }


    //----------------------------------------------------------------------------------------------
    // BUTTON HANDLERS
    //----------------------------------------------------------------------------------------------
    public void buttonPress(View view) {
        switch(view.getId()) {
            case R.id.edit_event_button:
                Intent intent = new Intent(this, EditEventActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("Event", event);
                intent.putExtras(bundle);
                startActivity(intent);
                break;
            case R.id.delete_event_button:
                DeleteEventDialogFragment deleteDialog = new DeleteEventDialogFragment();
                deleteDialog.show(getSupportFragmentManager(), "delete");
                break;
            case R.id.leave_event_button:
                mInterface.leaveEvent(Integer.parseInt(event.pk));
                break;
            case R.id.rsvp_event_button:
                mInterface.joinEvent(Integer.parseInt(event.pk));
                break;
            case R.id.view_host_button_going:
                Intent intent1 = new Intent(this, ViewProfileActivity.class);
                Bundle bundle1 = new Bundle();
                bundle1.putSerializable("Event", event);
                intent1.putExtras(bundle1);
                startActivity(intent1);
                break;
            case R.id.view_host_button_not_going:
                Intent intent2 = new Intent(this, ViewProfileActivity.class);
                Bundle bundle2 = new Bundle();
                bundle2.putSerializable("Event",event);
                intent2.putExtras(bundle2);
                startActivity(intent2);
                break;
            default:
                break;
        }
    }

    private String getCategoryName(Event event) {
        String categoryName;
        switch (event.getCategory()) {
            case "SP":
                categoryName = "Sports";
                break;
            case "MU":
                categoryName = "Music";
                break;
            case "VG":
                categoryName = "Video Games";
                break;
            case "MV":
                categoryName = "Movie";
                break;
            case "ST":
                categoryName = "Study";
                break;
            default:
                categoryName = "No Category";
                break;
        }
        return categoryName;
    }

    private class MyLocationViewer implements View.OnClickListener {
        private TextView _text;
        private Activity act;

        public MyLocationViewer(Context context, int editTextViewID) {
            act = (Activity) context;
            _text = (TextView) act.findViewById(editTextViewID);
            _text.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("Event", event);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    public String getDate(double date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        return sdf.format(new Date(Double.valueOf(date).longValue() * 1000));
    }

    public String getTime(double time) {
        SimpleDateFormat sdf = new SimpleDateFormat("H:mm", Locale.getDefault());
        return sdf.format(new Date(Double.valueOf(time).longValue() * 1000));
    }

    @Override
    public void deleteEvent(DialogFragment dialog) {
        mInterface.deleteEvent(Integer.parseInt(event.pk));
    }

    private void setImage(Event event) {
        if (event.getPhoto() != "") {
            mInterface.getEventPhoto(Integer.parseInt(this.event.pk));
        }
        else {
            switch (event.getCategory()) {
                case "SP":
                    eventPhoto.setImageResource(R.drawable.sports);
                    break;
                case "MU":
                    eventPhoto.setImageResource(R.drawable.music);
                    break;
                case "VG":
                    eventPhoto.setImageResource(R.drawable.videogames);
                    break;
                case "MV":
                    eventPhoto.setImageResource(R.drawable.movie);
                    break;
                case "ST":
                    eventPhoto.setImageResource(R.drawable.study);
                    break;
                default:
                    eventPhoto.setImageResource(R.drawable.sports);
                    break;
            }
        }
    }

    private class CommunicationHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch(msg.what) {
                case HandlerMessageConstants.MESSAGE_EVENT_RESPONSE:
                    if (msg.arg1 == HandlerMessageConstants.REQUEST_JOIN) {
                        if (msg.arg2 == HandlerMessageConstants.OPERATION_SUCCESS) {
                            toast = Toast.makeText(getApplicationContext(), "Successfully joined event", Toast.LENGTH_LONG);
                            toast.show();
                            finish();
                        }
                    }
                    else if (msg.arg1 == HandlerMessageConstants.REQUEST_LEAVE) {
                        if (msg.arg2 == HandlerMessageConstants.OPERATION_SUCCESS) {
                            toast = Toast.makeText(getApplicationContext(), "Successfully left event", Toast.LENGTH_LONG);
                            toast.show();
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intent.putExtra(MainActivity.UPDATE_RSVP, true);
                            startActivity(intent);
                            finish();
                        }
                    }
                    else if (msg.arg1 == HandlerMessageConstants.REQUEST_DELETE) {
                        if (msg.arg2 == HandlerMessageConstants.OPERATION_SUCCESS) {
                            toast = Toast.makeText(getApplicationContext(), "Successfully deleted event", Toast.LENGTH_LONG);
                            toast.show();
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                        }
                    }
                    break;
                case HandlerMessageConstants.MESSAGE_EVENT_PHOTO_RESPONSE:
                    if (msg.arg1 == HandlerMessageConstants.REQUEST_GET) {
                        if (msg.arg2 == HandlerMessageConstants.OPERATION_SUCCESS) {
                            // Set event photo
                            InputStream[] bytes = ((InputStream[]) msg.obj);
                            Bitmap bm = BitmapFactory.decodeStream(bytes[0]);
                            eventPhoto.setImageBitmap(bm);
                        }
                    }
                default:
                    break;
            }
        }
    }
}

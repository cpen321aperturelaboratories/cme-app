package com.example.sawye.cme_app;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.sawye.cme_app.control.FileUtils;
import com.example.sawye.cme_app.model.CMe;
import com.example.sawye.cme_app.model.Event;
import com.example.sawye.cme_app.remote.HandlerMessageConstants;
import com.example.sawye.cme_app.remote.ServerInterface;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by sawyer on 10/11/2017.
 */

public class CreateEventActivity extends AppCompatActivity {

    private static final int PICK_IMAGE = 1;
    private static final int PICK_LOCATION = 2;
    private ServerInterface mInterface;
    private Handler mHandler;
    private EditText eventName;
    private Spinner category;
    private TextView location;
    private TextView startDate;
    private TextView startTime;
    private TextView endDate;
    private TextView endTime;
    private EditText details;
    private TextView error_message;

    private MyDatePicker startDatePicker;
    private MyTimePicker startTimePicker;
    private MyDatePicker endDatePicker;
    private MyTimePicker endTimePicker;
    private LatLng latlng;

    private Event newEvent;
    private boolean imgSelected = false;
    private File imgFile;

    public static Toast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.activity_create_event_toolbar);
        setSupportActionBar(myToolbar);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);

        // Set views
        eventName = (EditText) findViewById(R.id.name_field);
        category = (Spinner) findViewById(R.id.category_field);
        location = (TextView) findViewById(R.id.location_field);
        startDate = (TextView) findViewById(R.id.start_date_field);
        startTime = (TextView) findViewById(R.id.start_time_field);
        endDate = (TextView) findViewById(R.id.end_date_field);
        endTime = (TextView) findViewById(R.id.end_time_field);
        details = (EditText) findViewById(R.id.details_field);
        error_message = (TextView) findViewById(R.id.error_message);
        details.setHorizontallyScrolling(false);
        details.setLines(3);

        startDatePicker = new MyDatePicker(this, R.id.start_date_field);
        startTimePicker = new MyTimePicker(this, R.id.start_time_field);
        endDatePicker = new MyDatePicker(this, R.id.end_date_field);
        endTimePicker = new MyTimePicker(this, R.id.end_time_field);
        MyLocationPicker locationPicker = new MyLocationPicker(this, R.id.location_field);

        // Handler
        mHandler = new CreateEventActivity.CommunicationHandler();

        // Server interface
        mInterface = ServerInterface.getInstance();
        mInterface.setHandler(mHandler);

        // Category drop down selector
        Spinner dropdown = (Spinner)findViewById(R.id.category_field);
        String[] items = new String[]{"Sports", "Music", "Video Games", "Movie", "Study"};
        // Create an adapter to describe how the items are displayed, adapters are used in several places in android.
        // There are multiple variations of this, but this is the basic variant.
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        // Set the spinners adapter to the previously created one.
        dropdown.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_event, menu);
        return true;
    }


    //----------------------------------------------------------------------------------------------
    // BUTTON HANDLERS
    //----------------------------------------------------------------------------------------------
    public void buttonPress(View view) {
        switch (view.getId()) {
            case R.id.create_event_button:
                // Check if any fields are empty
                String eventNameValue = eventName.getText().toString();
                String categoryValueFull = category.getSelectedItem().toString();
                String categoryValue;
                switch (categoryValueFull){
                    case "Sports":
                        categoryValue = "SP";
                        break;
                    case "Music":
                        categoryValue = "MU";
                        break;
                    case "Video Games":
                        categoryValue = "VG";
                        break;
                    case "Movie":
                        categoryValue = "MV";
                        break;
                    case "Study":
                        categoryValue = "ST";
                        break;
                    default:
                        categoryValue = "SP";
                        break;
                }

                String locationValue = location.getText().toString();
                String startDateValue = startDate.getText().toString();
                String startTimeValue = startTime.getText().toString();
                String endDateValue = endDate.getText().toString();
                String endTimeValue = endTime.getText().toString();
                String detailsValue = details.getText().toString();

                Log.i("date", startTimeValue);
                Log.i("date", startDateValue);
                Log.i("date", endTimeValue);
                Log.i("date", endDateValue);
                boolean error = false;

                if (eventNameValue.isEmpty()) {
                    eventName.setHintTextColor(getResources().getColor(R.color.red));
                    error = true;
                }
                if (getResources().getString(R.string.location_field).equals(locationValue)) {
                    location.setTextColor(getResources().getColor(R.color.red));
                    error = true;
                }
                if (getResources().getString(R.string.start_date_field).equals(startDateValue)) {
                    startDate.setTextColor(getResources().getColor(R.color.red));
                    error = true;
                }
                if (getResources().getString(R.string.start_time_field).equals(startTimeValue)) {
                    startTime.setTextColor(getResources().getColor(R.color.red));
                    error = true;
                }
                if (getResources().getString(R.string.end_date_field).equals(endDateValue)) {
                    endDate.setTextColor(getResources().getColor(R.color.red));
                    error = true;
                }
                if (getResources().getString(R.string.end_time_field).equals(endTimeValue)) {
                    endTime.setTextColor(getResources().getColor(R.color.red));
                    error = true;
                }
                if (detailsValue.isEmpty()) {
                    details.setHintTextColor(getResources().getColor(R.color.red));
                    error = true;
                }
                if (error) {
                    error_message.setText(R.string.field_missing_error);
                    error_message.setVisibility(View.VISIBLE);
                    break;
                }

                double eventStart = setDate(startDatePicker.getYear(), startDatePicker.getMonth(), startDatePicker.getDay(),
                        startTimePicker.getHour(), startTimePicker.getMinute());
                double eventEnd = setDate(endDatePicker.getYear(), endDatePicker.getMonth(), endDatePicker.getDay(),
                        endTimePicker.getHour(), endTimePicker.getMinute());
                double eventLongitude = latlng.longitude;
                double eventLatitude = latlng.latitude;
                String eventAddress = location.getText().toString();

                newEvent = new Event(mInterface.userId, mInterface.profileId, eventNameValue, categoryValue, detailsValue,
                                        eventStart, eventEnd, eventLatitude, eventLongitude, eventAddress, null, null);

                CMe<Event> cmeEvent = new CMe<>("cme.event", null, newEvent);
                List<CMe<Event>> eventList = new ArrayList<>();
                eventList.add(cmeEvent);
                mInterface.createEvent(eventList);
                break;
            case R.id.select_image_button:
                Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                getIntent.setType("image/*");
                Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                pickIntent.setType("image/*");
                Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});
                    startActivityForResult(Intent.createChooser(chooserIntent, "Select Picture"), PICK_IMAGE);
                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK) {
            if (data == null) {
                return;
            }
            try {
                ImageView imgViewer = (ImageView) findViewById(R.id.select_image_button);
                InputStream inputStream = getContentResolver().openInputStream(data.getData());
                Bitmap bm = BitmapFactory.decodeStream(inputStream);
                imgViewer.setImageBitmap(bm);

                Uri selectedImage = data.getData();
                String path = FileUtils.getPath(this, selectedImage);
                imgFile = new File(path);
                imgSelected = true;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        if (requestCode == PICK_LOCATION && resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);
                latlng = place.getLatLng();
                location.setText(place.getAddress());
                location.setTextColor(getResources().getColor(R.color.black));
        }
    }


    //----------------------------------------------------------------------------------------------
    // CONTROL HANDLERS
    //----------------------------------------------------------------------------------------------
    private class MyDatePicker implements View.OnClickListener, DatePickerDialog.OnDateSetListener {
        private TextView _text;
        private int _day;
        private int _month;
        private int _year;
        private Context _context;

        public MyDatePicker(Context context, int editTextViewID) {
            Activity act = (Activity) context;
            _text = (TextView) act.findViewById(editTextViewID);
            _text.setOnClickListener(this);
            this._context = context;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            _year = year;
            _month = monthOfYear;
            _day = dayOfMonth;
            updateDisplay();
        }

        public int getDay() { return _day; }

        public int getMonth() { return _month; }

        public int getYear() { return _year; }

        @Override
        public void onClick(View v) {
            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());

            DatePickerDialog dialog = new DatePickerDialog(_context, R.style.DialogTheme, this,
                    calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH));
            dialog.show();
        }

        // Updates the date in the EditText
        private void updateDisplay() {
            _text.setText(new StringBuilder()
                    // Month is 0 based so add 1
                    .append(_day).append("/").append(_month + 1).append("/").append(_year).append(" "));
            _text.setTextColor(getResources().getColor(R.color.black));
        }
    }

    private class MyTimePicker implements View.OnClickListener, TimePickerDialog.OnTimeSetListener {
        private TextView _text;
        private int _hour;
        private int _minute;
        private Context _context;

        public MyTimePicker(Context context, int editTextViewID) {
            Activity act = (Activity)context;
            _text = (TextView) act.findViewById(editTextViewID);
            _text.setOnClickListener(this);
            this._context = context;
        }

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            _hour = hourOfDay;
            _minute = minute;
            updateDisplay();
        }

        public int getHour() { return _hour; }

        public int getMinute() { return _minute; }

        @Override
        public void onClick(View v) {
            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            TimePickerDialog dialog = new TimePickerDialog(_context, R.style.DialogTheme, this,
                    calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false);
            dialog.show();
        }

        // Updates the time in the EditText
        private void updateDisplay() {
            if (_minute >= 10) {
                _text.setText(new StringBuilder().append(_hour).append(":").append(_minute));
            } else {
                _text.setText(new StringBuilder().append(_hour).append(":").append("0").append(_minute));
            }
            _text.setTextColor(getResources().getColor(R.color.black));
        }
    }

    private class MyLocationPicker implements View.OnClickListener {
        private TextView _text;
        private Context _context;
        private Activity act;

        public MyLocationPicker(Context context, int editTextViewID) {
            act = (Activity) context;
            _text = (TextView) act.findViewById(editTextViewID);
            _text.setOnClickListener(this);
            _context = context;
        }

        @Override
        public void onClick(View v) {
            int status = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(_context);
            if (status == ConnectionResult.SUCCESS) {
                PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                try {
                    startActivityForResult(builder.build(act), PICK_LOCATION);
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }
            }
            else {
                if (GoogleApiAvailability.getInstance().isUserResolvableError(status)) {
                    GoogleApiAvailability.getInstance().getErrorDialog(act, status, PICK_LOCATION).show();
                }
            }
        }
    }

    public double setDate(int year, int month, int day, int hour, int minute) {
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        calendar.set(year, month, day, hour, minute, 0);
        return calendar.getTimeInMillis() / 1000;
    }

    private class CommunicationHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch(msg.what) {
                case HandlerMessageConstants.MESSAGE_EVENT_RESPONSE:
                    if (msg.arg2 == HandlerMessageConstants.OPERATION_SUCCESS) {
                        if (imgSelected) {
                            mInterface.uploadEventPhoto(mInterface.eventId, imgFile);
                        }

                        toast = Toast.makeText(getApplicationContext(), "Event Created!", Toast.LENGTH_LONG);
                        toast.show();
                        finish();
                    }
                    else if (msg.arg2 == HandlerMessageConstants.OPERATION_FAILURE_UNPROCESSABLE) {
                        error_message.setText("Unprocessable entry");
                        error_message.setVisibility(View.VISIBLE);
                    }
                    break;
                case HandlerMessageConstants.MESSAGE_EVENT_PHOTO_RESPONSE:
                    if (msg.arg2 == HandlerMessageConstants.OPERATION_SUCCESS) {}
                    break;
                default:
                    break;
            }
        }
    }
}
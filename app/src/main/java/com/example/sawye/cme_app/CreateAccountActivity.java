package com.example.sawye.cme_app;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sawye.cme_app.model.CMe;
import com.example.sawye.cme_app.model.User;
import com.example.sawye.cme_app.remote.HandlerMessageConstants;
import com.example.sawye.cme_app.remote.ServerInterface;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sawye on 10/27/2017.
 */

public class CreateAccountActivity extends AppCompatActivity {


    //----------------------------------------------------------------------------------------------
    // THINGS
    //----------------------------------------------------------------------------------------------
    public static CreateAccountActivity ref;
    private TextView error_message;
    private EditText username;
    private ServerInterface mInterface;
    private Toast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ref = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.activity_create_activity_toolbar);
        setSupportActionBar(myToolbar);

        ///////////////////////////
        // Communication Handler //
        ///////////////////////////
        Handler mHandler = new CreateAccountActivity.CommunicationHandler();

        mInterface = ServerInterface.getInstance();
        mInterface.setHandler(mHandler);
    }


    //----------------------------------------------------------------------------------------------
    // BUTTON HANDLERS
    //----------------------------------------------------------------------------------------------
    public void buttonPress(View view) {
        switch (view.getId()) {
            case R.id.create_account_button:
                // Check if all fields are correct
                error_message = (TextView) findViewById(R.id.error_message_field);
                username = (EditText) findViewById(R.id.username_field);
                EditText firstName = (EditText) findViewById(R.id.first_name_field);
                EditText lastName = (EditText) findViewById(R.id.last_name_field);
                EditText email = (EditText) findViewById(R.id.email_field);
                EditText password = (EditText) findViewById(R.id.password_field);
                EditText confirmPassword = (EditText) findViewById(R.id.confirm_password_field);
                String usernameValue = username.getText().toString();
                String firstNameValue = firstName.getText().toString();
                String lastNameValue = lastName.getText().toString();
                String emailValue = email.getText().toString();
                String passwordValue = password.getText().toString();
                String confirmPasswordValue = confirmPassword.getText().toString();

                boolean error = false;
                if (usernameValue.isEmpty()) {
                    error_message.setText(R.string.field_missing_error);
                    error_message.setVisibility(View.VISIBLE);
                    username.setHintTextColor(getResources().getColor(R.color.red));
                    error = true;
                }
                if (firstNameValue.isEmpty()) {
                    error_message.setText(R.string.field_missing_error);
                    error_message.setVisibility(View.VISIBLE);
                    firstName.setHintTextColor(getResources().getColor(R.color.red));
                    error = true;
                }
                if (lastNameValue.isEmpty()) {
                    error_message.setText(R.string.field_missing_error);
                    error_message.setVisibility(View.VISIBLE);
                    lastName.setHintTextColor(getResources().getColor(R.color.red));
                    error = true;
                }
                if (emailValue.isEmpty()) {
                    error_message.setText(R.string.field_missing_error);
                    error_message.setVisibility(View.VISIBLE);
                    email.setHintTextColor(getResources().getColor(R.color.red));
                    error = true;
                }
                if (passwordValue.isEmpty()) {
                    error_message.setText(R.string.field_missing_error);
                    error_message.setVisibility(View.VISIBLE);
                    password.setHintTextColor(getResources().getColor(R.color.red));
                    password.setText("");
                    confirmPassword.setHintTextColor(getResources().getColor(R.color.red));
                    confirmPassword.setText("");
                    error = true;
                }
                if (confirmPasswordValue.isEmpty()) {
                    error_message.setText(R.string.field_missing_error);
                    error_message.setVisibility(View.VISIBLE);
                    password.setHintTextColor(getResources().getColor(R.color.red));
                    password.setText("");
                    confirmPassword.setHintTextColor(getResources().getColor(R.color.red));
                    confirmPassword.setText("");
                    error = true;
                }
                if (!passwordValue.equals(confirmPasswordValue)) {
                    password.setText("");
                    confirmPassword.setText("");
                    password.setHintTextColor(getResources().getColor(R.color.red));
                    confirmPassword.setHintTextColor(getResources().getColor(R.color.red));
                    if (!error) {
                        error_message.setText(R.string.password_not_match_error);
                        error_message.setVisibility(View.VISIBLE);
                    }
                    error = true;
                }
                if (error) break;

                User user = new User(usernameValue, firstNameValue,lastNameValue, emailValue, passwordValue);
                CMe<User> cmeUser = new CMe<>("auth.user", null, user);
                List<CMe<User>> userList = new ArrayList<>();
                userList.add(cmeUser);
                mInterface.createUser(userList);
                break;
            default:
                break;
        }
    }

    private class CommunicationHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case HandlerMessageConstants.MESSAGE_USER_RESPONSE:
                    if (msg.arg1 == HandlerMessageConstants.REQUEST_CREATE) {
                        if (msg.arg2 == HandlerMessageConstants.OPERATION_SUCCESS) {
                            toast = Toast.makeText(getApplicationContext(), "Created account. Please log in", Toast.LENGTH_LONG);
                            toast.show();
                            finish();
                        }
                        else if (msg.arg2 == HandlerMessageConstants.OPERATION_FAILURE_UNPROCESSABLE) {
                            ref.username.setHintTextColor(getResources().getColor(R.color.red));
                            ref.username.setText("");
                            ref.error_message.setText(R.string.account_already_exists);
                            ref.error_message.setVisibility(View.VISIBLE);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }
}

package com.example.sawye.cme_app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sawye.cme_app.remote.HandlerMessageConstants;
import com.example.sawye.cme_app.remote.ServerInterface;

/**
 * Created by sawyer on 10/11/2017.
 */

public class SettingsActivity extends AppCompatActivity implements DeleteAccountDialogFragment.DeleteAccountDialogListener,
                                                                   LogoutDialogFragment.LogoutDialogListener {

    private SeekBar searchRadius;
    private CheckBox sportsCheckbox;
    private CheckBox musicCheckbox;
    private CheckBox videogamesCheckbox;
    private CheckBox movieCheckbox;
    private CheckBox studyCheckbox;
    private SeekBar radiusBar;
    private TextView distanceSetting;

    private boolean sportsCheckboxState;
    private boolean musicCheckboxState;
    private boolean videogamesCheckboxState;
    private boolean movieCheckboxState;
    private boolean studyCheckboxState;
    private int radiusBarValue;

    private ServerInterface mInterface;

    public static Toast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.activity_settings_toolbar);
        setSupportActionBar(myToolbar);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);

        sportsCheckbox = (CheckBox) findViewById(R.id.sports_checkbox);
        musicCheckbox = (CheckBox) findViewById(R.id.music_checkbox);
        videogamesCheckbox = (CheckBox) findViewById(R.id.videogames_checkbox);
        movieCheckbox = (CheckBox) findViewById(R.id.movie_checkbox);
        studyCheckbox = (CheckBox) findViewById(R.id.study_checkbox);
        radiusBar = (SeekBar) findViewById(R.id.distance_bar);
        distanceSetting = (TextView) findViewById(R.id.search_distance_setting);
        distanceSetting.setText(Integer.toString(State.radiusBarValue) + "km");
        radiusBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener(){
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int distance = seekBar.getProgress();
                distanceSetting.setText(Integer.toString(distance) + "km");
            }
        });

        TextView versionText = (TextView) findViewById(R.id.version_text);
        String version = null;
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        versionText.setText("Cme Version " + version);

        // Handler
        Handler mHandler = new SettingsActivity.CommunicationHandler();

        // Server interface
        mInterface = ServerInterface.getInstance();
        mInterface.setHandler(mHandler);

        fetchSettings();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }


    //----------------------------------------------------------------------------------------------
    // BUTTON HANDLERS
    //----------------------------------------------------------------------------------------------
    public void buttonPress(View view) {
        switch(view.getId()) {
            case R.id.logout_button:
                LogoutDialogFragment logoutDialog = new LogoutDialogFragment();
                logoutDialog.show(getSupportFragmentManager(), "logout");
                break;
            case R.id.delete_account_button:
                DeleteAccountDialogFragment deleteDialog = new DeleteAccountDialogFragment();
                deleteDialog.show(getSupportFragmentManager(), "delete");
                break;
            case R.id.apply_button:
                updateSettings();
                finish();
                break;
            default:
                break;
        }
    }

    private class CommunicationHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch(msg.what) {
                case HandlerMessageConstants.MESSAGE_LOGOUT_RESPONSE:
                    if (msg.arg2 == HandlerMessageConstants.OPERATION_SUCCESS) {
                        toast = Toast.makeText(getApplicationContext(), "Successfully logged out", Toast.LENGTH_LONG);
                        toast.show();
                        returnToLogin();
                    }
                    break;
                case HandlerMessageConstants.MESSAGE_USER_RESPONSE:
                    if (msg.arg1 == HandlerMessageConstants.REQUEST_DELETE) {
                        if (msg.arg2 == HandlerMessageConstants.OPERATION_SUCCESS) {
                            toast = Toast.makeText(getApplicationContext(), "Successfully deleted account", Toast.LENGTH_LONG);
                            toast.show();
                            returnToLogin();
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }

    private void updateSettings() {
        sportsCheckboxState = sportsCheckbox.isChecked();
        musicCheckboxState = musicCheckbox.isChecked();
        videogamesCheckboxState = videogamesCheckbox.isChecked();
        movieCheckboxState = movieCheckbox.isChecked();
        studyCheckboxState = studyCheckbox.isChecked();
        radiusBarValue = radiusBar.getProgress();

        State.sportsCheckboxState = sportsCheckboxState;
        State.musicCheckboxState = musicCheckboxState;
        State.videogamesCheckboxState = videogamesCheckboxState;
        State.movieCheckboxState = movieCheckboxState;
        State.studyCheckboxState = studyCheckboxState;
        State.radiusBarValue = radiusBarValue;

        // Update category list
        State.updateState();

        SharedPreferences mPref = getSharedPreferences("settings", 0);
        SharedPreferences.Editor mEditor = mPref.edit();
        mEditor.putBoolean("sportsPref", State.sportsCheckboxState).commit();
        mEditor.putBoolean("musicPref", State.musicCheckboxState).commit();
        mEditor.putBoolean("videogamePref", State.videogamesCheckboxState).commit();
        mEditor.putBoolean("moviePref", State.movieCheckboxState).commit();
        mEditor.putBoolean("studyPref", State.studyCheckboxState).commit();
        mEditor.putInt("radiusPref", State.radiusBarValue).commit();
    }

    private void fetchSettings() {
        sportsCheckboxState = State.sportsCheckboxState;
        musicCheckboxState = State.musicCheckboxState;
        videogamesCheckboxState = State.videogamesCheckboxState;
        movieCheckboxState = State.movieCheckboxState;
        studyCheckboxState = State.studyCheckboxState;
        radiusBarValue = State.radiusBarValue;

        sportsCheckbox.setChecked(sportsCheckboxState);
        musicCheckbox.setChecked(musicCheckboxState);
        videogamesCheckbox.setChecked(videogamesCheckboxState);
        movieCheckbox.setChecked(movieCheckboxState);
        studyCheckbox.setChecked(studyCheckboxState);
        radiusBar.setProgress(radiusBarValue);
    }

    @Override
    public void logout(DialogFragment dialog) {
        mInterface.logout();
    }

    @Override
    public void deleteAccount(DialogFragment dialog) {
        mInterface.deleteUser(mInterface.userId);
    }

    public void returnToLogin() {
        // Return to login screen
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.putExtra(MainActivity.FINISH_THIS, true);
        startActivity(intent);
        finish();
    }
}

package com.example.sawye.cme_app.remote;

import android.os.AsyncTask;
import android.util.Log;

import com.example.sawye.cme_app.api.APIEndpoint;
import com.example.sawye.cme_app.model.StdResponse;
import com.example.sawye.cme_app.model.Event;
import com.example.sawye.cme_app.model.CMe;
import com.example.sawye.cme_app.model.Id;
import com.example.sawye.cme_app.model.Profile;
import com.example.sawye.cme_app.model.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.os.Message;
import android.os.Handler;

/**
 * Created by JR on 10/14/2017.
 */

public class ServerInterface {


    //----------------------------------------------------------------------------------------------
    // THINGS
    //----------------------------------------------------------------------------------------------
    private final int UNAUTHORIZED  = 401;
    private final int FORBIDDEN     = 403;
    private final int NOT_FOUND     = 404;
    private final int UNPROCESSABLE = 422;

    private APIEndpoint api;
    public int userId, profileId, eventId;
    private Handler mHandler;

    private static volatile ServerInterface instance;

    // Private Constructor
    private ServerInterface() {
        api = ApiUtils.getAPIEndpoint();

        // Prevent form the reflection api
        if (instance != null) {
            throw new ExceptionInInitializerError(
                    "Use getInstance() method to get the single instance of this class.");
        }
    }

    public static ServerInterface getInstance() {
        // Double check locking pattern
        if (instance == null) {                     // Check for the first time
            synchronized (ServerInterface.class) {  // Check for the second time
                // If there is no instance available...create new one
                if (instance == null) instance = new ServerInterface();
            }
        }
        return instance;
    }

    public void setHandler(Handler handler) {
        mHandler = handler;
    }


    //----------------------------------------------------------------------------------------------
    // STDRESPONSE HANDLER
    //----------------------------------------------------------------------------------------------
    private void stdResponse(Response<ResponseBody> response, int responseType, int request, int reason) {
        Gson gson = new Gson();
        StdResponse error = null;
        try {
            error = gson.fromJson(response.errorBody().string(), StdResponse.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Message readMsg = mHandler.obtainMessage(responseType, request, reason, error);
        readMsg.sendToTarget();
    }


    //----------------------------------------------------------------------------------------------
    // AUTHENTICATION HANDLERS
    //----------------------------------------------------------------------------------------------
    public void login(String username, String password) {
        api.login(username, password).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        // Save IDs
                        Gson gson = new Gson();
                        Id id = gson.fromJson(response.body().string(), Id.class);
                        userId = id.getUserId();
                        profileId = id.getProfileId();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    // Tell user we successfully logged in
                    Message readMsg = mHandler.obtainMessage(
                            HandlerMessageConstants.MESSAGE_LOGIN_RESPONSE,
                            0,
                            HandlerMessageConstants.OPERATION_SUCCESS);
                    readMsg.sendToTarget();
                }
                else if (response.code() == UNAUTHORIZED) {
                    // Tell user login was not successful
                    stdResponse(response,
                            HandlerMessageConstants.MESSAGE_LOGIN_RESPONSE,
                            0,
                            HandlerMessageConstants.OPERATION_FAILURE_UNAUTHORIZED);
                }
                else {
                    // Tell user that no account was associated with those credentials
                    Message readMsg = mHandler.obtainMessage(
                            HandlerMessageConstants.MESSAGE_LOGIN_RESPONSE,
                            0,
                            HandlerMessageConstants.OPERATION_FAILURE_SERVER_ERROR);
                    readMsg.sendToTarget();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Login Error", t.getMessage());
            }
        });
    }

    public void logout() {
        api.logout().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    // Tell user we successfully logged out
                    Message readMsg = mHandler.obtainMessage(
                            HandlerMessageConstants.MESSAGE_LOGOUT_RESPONSE,
                            0,
                            HandlerMessageConstants.OPERATION_SUCCESS);
                    readMsg.sendToTarget();
                }
                else {
                    try {
                        Log.e("Logout Response", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Logout Error", t.getMessage());
            }
        });
    }


    //----------------------------------------------------------------------------------------------
    // USER HANDLERS
    //----------------------------------------------------------------------------------------------
    public void createUser(List<CMe<User>> user) {
        api.createUser(user).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        // Save IDs
                        Gson gson = new Gson();
                        Id id = gson.fromJson(response.body().string(), Id.class);
                        userId = id.getUserId();
                        profileId = id.getProfileId();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    // Tell user we successfully created their details
                    Message readMsg = mHandler.obtainMessage(
                            HandlerMessageConstants.MESSAGE_USER_RESPONSE,
                            HandlerMessageConstants.REQUEST_CREATE,
                            HandlerMessageConstants.OPERATION_SUCCESS);
                    readMsg.sendToTarget();
                }
                else if (response.code() == UNPROCESSABLE) {
                    // Tell user there was an error with an input
                    stdResponse(response,
                            HandlerMessageConstants.MESSAGE_USER_RESPONSE,
                            HandlerMessageConstants.REQUEST_CREATE,
                            HandlerMessageConstants.OPERATION_FAILURE_UNPROCESSABLE);
                }
                else {
                    try {
                        Log.e("Create User Response", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Create User Error", t.getMessage());
            }
        });
    }

    public void deleteUser(Integer user_id) {
        api.deleteUser(user_id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    // Tell user we successfully deleted their account
                    Message readMsg = mHandler.obtainMessage(
                            HandlerMessageConstants.MESSAGE_USER_RESPONSE,
                            HandlerMessageConstants.REQUEST_DELETE,
                            HandlerMessageConstants.OPERATION_SUCCESS);
                    readMsg.sendToTarget();
                }
                else {
                    try {
                        Log.e("Delete User Response", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Delete User Error", t.getMessage());
            }
        });
    }

    public void getUser(Integer user_id) {
        api.getUser(user_id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        Gson gson = new Gson();
                        Type fields = new TypeToken<List<CMe<User>>>(){}.getType();
                        List<CMe<User>> user = gson.fromJson(response.body().string(), fields);
                        // Send user details
                        Message readMsg = mHandler.obtainMessage(
                                HandlerMessageConstants.MESSAGE_USER_RESPONSE,
                                HandlerMessageConstants.REQUEST_GET,
                                HandlerMessageConstants.OPERATION_SUCCESS,
                                user);
                        readMsg.sendToTarget();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else if (response.code() == NOT_FOUND) {
                    // Tell user their details were not found
                    stdResponse(response,
                            HandlerMessageConstants.MESSAGE_USER_RESPONSE,
                            HandlerMessageConstants.REQUEST_GET,
                            HandlerMessageConstants.OPERATION_FAILURE_NOT_FOUND);
                }
                else {
                    try {
                        Log.e("User Response", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("User Error", t.getMessage());
            }
        });
    }


    public void getUserProfile(Integer user_id) {
        api.getUserProfile(user_id).enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.isSuccessful()) {
                    // Tell user their details were updated
                    Message readMsg = mHandler.obtainMessage(
                            HandlerMessageConstants.MESSAGE_PROFILE_RESPONSE,
                            HandlerMessageConstants.REQUEST_UPDATE,
                            HandlerMessageConstants.OPERATION_SUCCESS,
                            response.body());
                    readMsg.sendToTarget();
                }
                else {
                    try {
                        Log.e("User Profile Response", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                Log.e("User Profile Error", t.getMessage());
            }
        });
    }

    public void updateUser(Integer user_id, List<CMe<User>> user) {
        api.updateUser(user_id, user).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    // Tell user their details were updated
                    Message readMsg = mHandler.obtainMessage(
                            HandlerMessageConstants.MESSAGE_USER_RESPONSE,
                            HandlerMessageConstants.REQUEST_UPDATE,
                            HandlerMessageConstants.OPERATION_SUCCESS);
                    readMsg.sendToTarget();
                }
                else if (response.code() == UNAUTHORIZED) {
                    // Tell user updating was unsuccessful
                    stdResponse(response,
                            HandlerMessageConstants.MESSAGE_USER_RESPONSE,
                            HandlerMessageConstants.REQUEST_UPDATE,
                            HandlerMessageConstants.OPERATION_FAILURE_UNAUTHORIZED);
                }
                else if (response.code() == UNPROCESSABLE) {
                    // Tell user they cannot change their username
                    stdResponse(response,
                            HandlerMessageConstants.MESSAGE_USER_RESPONSE,
                            HandlerMessageConstants.REQUEST_UPDATE,
                            HandlerMessageConstants.OPERATION_FAILURE_UNPROCESSABLE);
                }
                else {
                    try {
                        Log.e("Update User Response", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Update User Error", t.getMessage());
            }
        });
    }


    //----------------------------------------------------------------------------------------------
    // PROFILE HANDLERS
    //----------------------------------------------------------------------------------------------
    public void getProfile(Integer profile_id) {
        api.getProfile(profile_id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        Gson gson = new Gson();
                        Type fields = new TypeToken<List<CMe<Profile>>>(){}.getType();
                        List<CMe<Profile>> profile = gson.fromJson(response.body().string(), fields);
                        // Send profile details
                        Message readMsg = mHandler.obtainMessage(
                                HandlerMessageConstants.MESSAGE_PROFILE_RESPONSE,
                                HandlerMessageConstants.REQUEST_GET,
                                HandlerMessageConstants.OPERATION_SUCCESS,
                                profile);
                        readMsg.sendToTarget();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else if (response.code() == NOT_FOUND) {
                    // Tell user profile details were not found
                    stdResponse(response,
                            HandlerMessageConstants.MESSAGE_PROFILE_RESPONSE,
                            HandlerMessageConstants.REQUEST_GET,
                            HandlerMessageConstants.OPERATION_FAILURE_NOT_FOUND);
                }
                else {
                    try {
                        Log.e("Profile Response", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Profile Error", t.getMessage());
            }
        });
    }

    public void updateProfile(Integer profile_id, List<CMe<Profile>> profile) {
        api.updateProfile(profile_id, profile).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    // Tell user their profile details were updated
                    Message readMsg = mHandler.obtainMessage(
                            HandlerMessageConstants.MESSAGE_PROFILE_RESPONSE,
                            HandlerMessageConstants.REQUEST_UPDATE,
                            HandlerMessageConstants.OPERATION_SUCCESS);
                    readMsg.sendToTarget();
                }
                else if (response.code() == FORBIDDEN) {
                    // Tell user updating profile was unsuccessful
                    stdResponse(response,
                            HandlerMessageConstants.MESSAGE_PROFILE_RESPONSE,
                            HandlerMessageConstants.REQUEST_UPDATE,
                            HandlerMessageConstants.OPERATION_FAILURE_FORBIDDEN);
                }
                else if (response.code() == UNPROCESSABLE) {
                    // Tell user there was an error with an input
                    stdResponse(response,
                            HandlerMessageConstants.MESSAGE_PROFILE_RESPONSE,
                            HandlerMessageConstants.REQUEST_UPDATE,
                            HandlerMessageConstants.OPERATION_FAILURE_UNPROCESSABLE);
                }
                else {
                    try {
                        Log.e("Update Profile Response", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Update Profile Error", t.getMessage());
            }
        });
    }

    public void uploadProfilePhoto(Integer profile_id, File file) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("photo", file.getName(), requestFile);

        api.uploadProfilePhoto(profile_id, body).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    // Tell user their profile photo was uploaded
                    Message readMsg = mHandler.obtainMessage(
                            HandlerMessageConstants.MESSAGE_PROFILE_PHOTO_RESPONSE,
                            HandlerMessageConstants.REQUEST_UPDATE,
                            HandlerMessageConstants.OPERATION_SUCCESS);
                    readMsg.sendToTarget();
                }
                else if (response.code() == NOT_FOUND) {
                    // Tell user profile was not found
                    stdResponse(response,
                            HandlerMessageConstants.MESSAGE_PROFILE_PHOTO_RESPONSE,
                            HandlerMessageConstants.REQUEST_UPDATE,
                            HandlerMessageConstants.OPERATION_FAILURE_NOT_FOUND);
                }
                else {
                    try {
                        Log.e("Upload Photo Error", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Upload Photo Error", t.getMessage());
            }
        });
    }

    public void getProfilePhoto(Integer profile_id) {
        api.getProfilePhoto(profile_id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    new AsyncTask<InputStream, Void, Void>() {
                        @Override
                        protected Void doInBackground(InputStream... params) {
                            // Send profile photo
                            Message readMsg = mHandler.obtainMessage(
                                    HandlerMessageConstants.MESSAGE_PROFILE_PHOTO_RESPONSE,
                                    HandlerMessageConstants.REQUEST_GET,
                                    HandlerMessageConstants.OPERATION_SUCCESS,
                                    params);
                            readMsg.sendToTarget();
                            return null;
                        }
                    }.execute(response.body().byteStream());
                }
                else if (response.code() == NOT_FOUND) {
                    // Tell user profile was not found
                    stdResponse(response,
                            HandlerMessageConstants.MESSAGE_PROFILE_PHOTO_RESPONSE,
                            HandlerMessageConstants.REQUEST_GET,
                            HandlerMessageConstants.OPERATION_FAILURE_NOT_FOUND);
                }
                else {
                    try {
                        Log.e("Profile Photo Response", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Profile Photo Error", t.getMessage());
            }
        });
    }


    //----------------------------------------------------------------------------------------------
    // EVENT HANDLERS
    //----------------------------------------------------------------------------------------------
    public void createEvent(List<CMe<Event>> event) {
        api.createEvent(event).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        // Save ID
                        Gson gson = new Gson();
                        StdResponse id = gson.fromJson(response.body().string(), StdResponse.class);
                        eventId = Integer.valueOf(id.getReturnsMessage());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    // Tell user their event was created
                    Message readMsg = mHandler.obtainMessage(
                            HandlerMessageConstants.MESSAGE_EVENT_RESPONSE,
                            HandlerMessageConstants.REQUEST_CREATE,
                            HandlerMessageConstants.OPERATION_SUCCESS);
                    readMsg.sendToTarget();
                }
                else if (response.code() == UNPROCESSABLE) {
                    // Tell user there was an error with an input
                    stdResponse(response,
                            HandlerMessageConstants.MESSAGE_EVENT_RESPONSE,
                            HandlerMessageConstants.REQUEST_CREATE,
                            HandlerMessageConstants.OPERATION_FAILURE_UNPROCESSABLE);
                }
                else {
                    try {
                        Log.e("Create Event Response", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Create Event Error", t.getMessage());
            }
        });
    }

    public void deleteEvent(Integer event_id) {
        api.deleteEvent(event_id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    // Tell user we successfully deleted their event
                    Message readMsg = mHandler.obtainMessage(
                            HandlerMessageConstants.MESSAGE_EVENT_RESPONSE,
                            HandlerMessageConstants.REQUEST_DELETE,
                            HandlerMessageConstants.OPERATION_SUCCESS);
                    readMsg.sendToTarget();
                }
                else {
                    try {
                        Log.e("Delete Event Response", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Delete Event Error", t.getMessage());
            }
        });
    }

    public void joinEvent(Integer event_id) {
        api.joinEvent(event_id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    // Tell user joining event was successful
                    Message readMsg = mHandler.obtainMessage(
                            HandlerMessageConstants.MESSAGE_EVENT_RESPONSE,
                            HandlerMessageConstants.REQUEST_JOIN,
                            HandlerMessageConstants.OPERATION_SUCCESS);
                    readMsg.sendToTarget();
                }
                else if (response.code() == FORBIDDEN) {
                    // Tell user joining event was unsuccessful
                    stdResponse(response,
                            HandlerMessageConstants.MESSAGE_EVENT_RESPONSE,
                            HandlerMessageConstants.REQUEST_JOIN,
                            HandlerMessageConstants.OPERATION_FAILURE_FORBIDDEN);
                }
                else if (response.code() == UNPROCESSABLE) {
                    // Tell user joining event was unsuccessful
                    stdResponse(response,
                            HandlerMessageConstants.MESSAGE_EVENT_RESPONSE,
                            HandlerMessageConstants.REQUEST_JOIN,
                            HandlerMessageConstants.OPERATION_FAILURE_UNPROCESSABLE);
                }
                else {
                    try {
                        Log.e("Join Event Response", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Join Event Error", t.getMessage());
            }
        });
    }

    public void leaveEvent(Integer event_id) {
        api.leaveEvent(event_id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    // Tell user their event was created
                    Message readMsg = mHandler.obtainMessage(
                            HandlerMessageConstants.MESSAGE_EVENT_RESPONSE,
                            HandlerMessageConstants.REQUEST_LEAVE,
                            HandlerMessageConstants.OPERATION_SUCCESS);
                    readMsg.sendToTarget();
                }
                else if (response.code() == FORBIDDEN) {
                    // Tell user leaving event was unsuccessful
                    stdResponse(response,
                            HandlerMessageConstants.MESSAGE_EVENT_RESPONSE,
                            HandlerMessageConstants.REQUEST_LEAVE,
                            HandlerMessageConstants.OPERATION_FAILURE_FORBIDDEN);
                }
                else if (response.code() == UNPROCESSABLE) {
                    // Tell user leaving event was unsuccessful
                    stdResponse(response,
                            HandlerMessageConstants.MESSAGE_EVENT_RESPONSE,
                            HandlerMessageConstants.REQUEST_LEAVE,
                            HandlerMessageConstants.OPERATION_FAILURE_UNPROCESSABLE);
                }
                else {
                    try {
                        Log.e("Leave Event Response", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Leave Event Error", t.getMessage());
            }
        });
    }

    public void getEventById(Integer event_id) {
        api.getEventById(event_id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        Gson gson = new Gson();
                        Type fields = new TypeToken<List<CMe<Event>>>(){}.getType();
                        List<CMe<Event>> event = gson.fromJson(response.body().string(), fields);
                        // Send event details
                        Message readMsg = mHandler.obtainMessage(
                                HandlerMessageConstants.MESSAGE_EVENT_RESPONSE,
                                HandlerMessageConstants.REQUEST_GET,
                                HandlerMessageConstants.OPERATION_SUCCESS,
                                event);
                        readMsg.sendToTarget();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (response.code() == UNPROCESSABLE) {
                    // Tell user event was not found
                    stdResponse(response,
                            HandlerMessageConstants.MESSAGE_EVENT_RESPONSE,
                            HandlerMessageConstants.REQUEST_GET,
                            HandlerMessageConstants.OPERATION_FAILURE_UNPROCESSABLE);
                } else {
                    try {
                        Log.e("Event ID Response", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Event ID Error", t.getMessage());
            }
        });
    }

    public void updateEvent(Integer event_id, List<CMe<Event>> event) {
        api.updateEvent(event_id, event).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    // Tell user their event details were updated
                    Message readMsg = mHandler.obtainMessage(
                            HandlerMessageConstants.MESSAGE_EVENT_RESPONSE,
                            HandlerMessageConstants.REQUEST_UPDATE,
                            HandlerMessageConstants.OPERATION_SUCCESS);
                    readMsg.sendToTarget();
                }
                else if (response.code() == FORBIDDEN) {
                    // Tell user updating event was unsuccessful
                    stdResponse(response,
                            HandlerMessageConstants.MESSAGE_EVENT_RESPONSE,
                            HandlerMessageConstants.REQUEST_UPDATE,
                            HandlerMessageConstants.OPERATION_FAILURE_FORBIDDEN);

                }
                else if (response.code() == UNPROCESSABLE) {
                    // Tell user there was an error with an input
                    stdResponse(response,
                            HandlerMessageConstants.MESSAGE_EVENT_RESPONSE,
                            HandlerMessageConstants.REQUEST_UPDATE,
                            HandlerMessageConstants.OPERATION_FAILURE_UNPROCESSABLE);

                }
                else {
                    try {
                        Log.e("Update Event Response", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Update Event Error", t.getMessage());
            }
        });
    }

    public void getEventsByCategories(List<String> category, String strategy, Integer offset, Integer size) {
        api.getEventsByCategories(category, strategy, offset, size).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        Gson gson = new Gson();
                        Type fields = new TypeToken<List<CMe<Event>>>(){}.getType();
                        List<CMe<Event>> events = gson.fromJson(response.body().string(), fields);
                        // Send event details
                        Message readMsg = mHandler.obtainMessage(
                                HandlerMessageConstants.MESSAGE_EVENTS_RESPONSE,
                                HandlerMessageConstants.REQUEST_GET,
                                HandlerMessageConstants.OPERATION_SUCCESS,
                                events);
                        readMsg.sendToTarget();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else if (response.code() == UNPROCESSABLE) {
                    // Tell user event was not found
                    stdResponse(response,
                            HandlerMessageConstants.MESSAGE_EVENTS_RESPONSE,
                            HandlerMessageConstants.REQUEST_GET,
                            HandlerMessageConstants.OPERATION_FAILURE_UNPROCESSABLE);
                }
                else {
                    try {
                        Log.e("Event Category Response", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Event Category Error", t.getMessage());
                Message readMsg = mHandler.obtainMessage(
                        HandlerMessageConstants.MESSAGE_EVENTS_RESPONSE,
                        HandlerMessageConstants.REQUEST_GET,
                        HandlerMessageConstants.OPERATION_FAILURE_SERVER_ERROR);
                readMsg.sendToTarget();
            }
        });
    }

    public void getEventsByOwner(Integer owner_id, String strategy, Integer offset, Integer size) {
        api.getEventsByOwner(owner_id, strategy, offset, size).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        Gson gson = new Gson();
                        Type fields = new TypeToken<List<CMe<Event>>>(){}.getType();
                        List<CMe<Event>> events = gson.fromJson(response.body().string(), fields);
                        // Send event details
                        Message readMsg = mHandler.obtainMessage(
                                HandlerMessageConstants.MESSAGE_EVENTS_ID_RESPONSE,
                                HandlerMessageConstants.REQUEST_GET,
                                HandlerMessageConstants.OPERATION_SUCCESS,
                                events);
                        readMsg.sendToTarget();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else if (response.code() == UNPROCESSABLE) {
                    // Tell user event was not found
                    stdResponse(response,
                            HandlerMessageConstants.MESSAGE_EVENTS_ID_RESPONSE,
                            HandlerMessageConstants.REQUEST_GET,
                            HandlerMessageConstants.OPERATION_FAILURE_UNPROCESSABLE);
                }
                else {
                    try {
                        Log.e("Event Owner Response", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Event Owner Error", t.getMessage());
            }
        });
    }

    public void uploadEventPhoto(Integer event_id, File file) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part body =
                MultipartBody.Part.createFormData("photo", file.getName(), requestFile);

        api.uploadEventPhoto(event_id, body).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    // Tell user their event photo was uploaded
                    Message readMsg = mHandler.obtainMessage(
                            HandlerMessageConstants.MESSAGE_EVENT_PHOTO_RESPONSE,
                            HandlerMessageConstants.REQUEST_UPDATE,
                            HandlerMessageConstants.OPERATION_SUCCESS);
                    readMsg.sendToTarget();
                }
                else if (response.code() == UNAUTHORIZED) {
                    // Tell user uploading was unsuccessful
                    stdResponse(response,
                            HandlerMessageConstants.MESSAGE_EVENT_PHOTO_RESPONSE,
                            HandlerMessageConstants.REQUEST_UPDATE,
                            HandlerMessageConstants.OPERATION_FAILURE_UNAUTHORIZED);
                }
                else if (response.code() == NOT_FOUND) {
                    // Tell user event was not found
                    stdResponse(response,
                            HandlerMessageConstants.MESSAGE_EVENT_PHOTO_RESPONSE,
                            HandlerMessageConstants.REQUEST_UPDATE,
                            HandlerMessageConstants.OPERATION_FAILURE_NOT_FOUND);
                }
                else {
                    try {
                        Log.e("Upload Photo Error", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Upload Photo Error", t.getMessage());
            }
        });
    }

    public void getEventPhoto(Integer event_id) {
        api.getEventPhoto(event_id).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    new AsyncTask<InputStream, Void, Void>() {
                        @Override
                        protected Void doInBackground(InputStream... params) {
                            // Send event photo
                            Message readMsg = mHandler.obtainMessage(
                                    HandlerMessageConstants.MESSAGE_EVENT_PHOTO_RESPONSE,
                                    HandlerMessageConstants.REQUEST_GET,
                                    HandlerMessageConstants.OPERATION_SUCCESS,
                                    params);
                            readMsg.sendToTarget();
                            return null;
                        }
                    }.execute(response.body().byteStream());
                }
                else if (response.code() == NOT_FOUND) {
                    // Tell user event was not found
                    stdResponse(response,
                            HandlerMessageConstants.MESSAGE_EVENT_PHOTO_RESPONSE,
                            HandlerMessageConstants.REQUEST_GET,
                            HandlerMessageConstants.OPERATION_FAILURE_NOT_FOUND);
                }
                else {
                    try {
                        Log.e("Event Photo Response", response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Event Photo Error", t.getMessage());
            }
        });
    }
}

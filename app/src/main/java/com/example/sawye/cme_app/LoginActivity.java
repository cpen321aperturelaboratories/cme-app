package com.example.sawye.cme_app;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sawye.cme_app.remote.HandlerMessageConstants;
import com.example.sawye.cme_app.remote.ServerInterface;

/**
 * Created by sawyer on 10/11/2017.
 */

public class LoginActivity extends AppCompatActivity {


    //----------------------------------------------------------------------------------------------
    // THINGS
    //----------------------------------------------------------------------------------------------
    public static LoginActivity ref;
    private EditText usernameEt;
    private EditText passwordEt;
    private TextView error_message;
    private ServerInterface mInterface;
    private Handler mHandler;

    public static Toast toast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ref = this;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ///////////////////////////
        // Communication Handler //
        ///////////////////////////
        mHandler = new LoginActivity.CommunicationHandler();

        usernameEt = (EditText) findViewById(R.id.username_field);
        passwordEt = (EditText) findViewById(R.id.password_field);
        error_message = (TextView) findViewById(R.id.error_message_field);

        mInterface = ServerInterface.getInstance();
        mInterface.setHandler(mHandler);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ref = this;
        mInterface.setHandler(mHandler);
    }


    //----------------------------------------------------------------------------------------------
    // BUTTON HANDLERS
    //----------------------------------------------------------------------------------------------
    public void buttonPress(View view) {
        switch (view.getId()) {
            case R.id.button_login:
                String username = usernameEt.getText().toString().trim();
                String password = passwordEt.getText().toString().trim();
                // Send username.getText().toString() and password.getText().toString() to server to verify

                if (username.isEmpty() && password.isEmpty()){
                    username = "cme";
                    password = "cme123cme123";
                    usernameEt.setText(username);
                    passwordEt.setText(password);
                }
                // Call login function
                mInterface.login(username, password);
                break;

            case R.id.create_account:
                Intent intent = new Intent(getApplicationContext(), CreateAccountActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }


    private class CommunicationHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case HandlerMessageConstants.MESSAGE_LOGIN_RESPONSE:
                    if (msg.arg2 == HandlerMessageConstants.OPERATION_SUCCESS) {
                        String username = ((EditText)findViewById(R.id.username_field)).getText().toString();
                        toast = Toast.makeText(getApplicationContext(),"Welcome " + username,Toast.LENGTH_LONG);
                        toast.show();
                        Intent intent1 = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent1);
                        finish();
                    }
                    else if (msg.arg2 == HandlerMessageConstants.OPERATION_FAILURE_UNAUTHORIZED) {
                        ref.usernameEt.setText("");
                        ref.passwordEt.setText("");
                        ref.error_message.setVisibility(View.VISIBLE);
                        ref.error_message.setText(R.string.login_incorrect_credentials_error);
                    }
                    else {
                        ref.usernameEt.setText("");
                        ref.passwordEt.setText("");
                        ref.error_message.setVisibility(View.VISIBLE);
                        ref.error_message.setText(R.string.login_profile_not_found_error);
                    }
                    break;
                default:
                    break;
            }
        }
    }
}








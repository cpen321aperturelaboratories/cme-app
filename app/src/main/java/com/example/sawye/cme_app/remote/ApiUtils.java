package com.example.sawye.cme_app.remote;

import com.example.sawye.cme_app.api.APIEndpoint;

/**
 * Created by JR on 10/14/2017.
 */

public class ApiUtils {

    // 10.0.2.2 is special alias to 127.0.0.1 (Android Emulator Networking)
    //public static final String BASE_URL = "http://10.0.2.2:8000/cme/";
    //public static final String BASE_URL = "http://192.168.0.104:8000/cme/";

    public static final String BASE_URL = "http://206.87.222.194:8000/cme/";
    //public static final String BASE_URL = "http://128.189.228.11:8000/cme/";

    public static APIEndpoint getAPIEndpoint() {
        RetrofitClient.init(BASE_URL);
        return RetrofitClient.getInstance().createAdapter().create(APIEndpoint.class);
    }
}

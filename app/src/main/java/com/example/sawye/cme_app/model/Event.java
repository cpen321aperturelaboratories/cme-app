package com.example.sawye.cme_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by zeyad on 10/16/17.
 */

public class Event implements Serializable {

    public enum Category {
        SPORTS("SP"),
        MUSIC("MU"),
        VIDEO_GAMES("VG"),
        MOVIE("MV"),
        STUDY("ST");

        private String code;

        Category(String code) {
            this.code = code;
        }

        public String getCode() { return code; }
    }

    @SerializedName("owner")
    @Expose
    private Integer owner;

    @SerializedName("owner_profile")
    @Expose
    private Integer ownerProfile;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("category")
    @Expose
    private String category;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("event_start")
    @Expose
    private double eventStart;

    @SerializedName("event_end")
    @Expose
    private double eventEnd;

    @SerializedName("latitude")
    @Expose
    private double latitude;

    @SerializedName("longitude")
    @Expose
    private double longitude;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("photo")
    @Expose
    private String photo;

    @SerializedName("participants")
    @Expose
    private ArrayList<Integer> participants;

    public Event(Integer owner, Integer ownerProfile, String name, String category, String description,
                 double eventStart, double eventEnd, double latitude, double longitude,
                 String address, String photo, ArrayList<Integer> participants) {
        this.owner = owner;
        this.ownerProfile = ownerProfile;
        this.name = name;
        this.category = category;
        this.description = description;
        this.eventStart = eventStart;
        this.eventEnd = eventEnd;
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
        this.photo = photo;
        this.participants = participants;
    }

    public Integer getOwner() {return owner;}

    public void setOwner(Integer owner) {
        this.owner = owner;
    }

    public Integer getOwnerProfile() { return ownerProfile; }

    public void setOwnerProfile(Integer ownerProfile) { this.ownerProfile = ownerProfile; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getEventStart() { return eventStart; }

    public void setEventStart(double eventStart) { this.eventStart = eventStart; }

    public double getEventEnd() { return eventEnd; }

    public void setEventEnd(double eventEnd) { this.eventEnd = eventEnd; }

    public double getLatitude() { return latitude; }

    public void setLatitude(double latitude) { this.latitude = latitude; }

    public double getLongitude() { return longitude; }

    public void setLongitude(double longitude) { this.longitude = longitude; }

    public String getAddress() { return address; }

    public void setAddress(String address) { this.address = address; }

    public String getPhoto() { return photo; }

    public void setPhoto(String photo) { this.photo = photo; }

    public ArrayList<Integer> getParticipants() {
        return participants;
    }

    public void setParticipants(ArrayList<Integer> participants) { this.participants = participants; }
}

package com.example.sawye.cme_app.model;

import com.example.sawye.cme_app.control.Parser;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by JR on 10/14/2017.
 */

public class Profile {

    @SerializedName("user")
    @Expose
    private Integer user;

    @SerializedName("photo")
    @Expose
    private String photo;

    @SerializedName("age")
    @Expose
    private Integer age;

    @SerializedName("bio")
    @Expose
    private String bio;

    @SerializedName("phone")
    @Expose
    private String phone;

    @SerializedName("gender")
    @Expose
    private String gender;

    @SerializedName("preferences")
    @Expose
    private String preferences;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("postal_code")
    @Expose
    private String postalCode;

    @SerializedName("rsvp_list")
    @Expose
    private ArrayList<String> rsvpList;

    public Profile(Integer user, String photo, Integer age, String bio, String phone,
                   String gender, String preferences, String address, String postalCode,
                    ArrayList<String> rsvpList) {
        this.user = user;
        this.photo = photo;
        this.age = age;
        this.bio = bio;
        this.phone = phone;
        this.gender = gender;
        this.preferences = preferences;
        this.address = address;
        this.postalCode = postalCode;
        this.rsvpList = rsvpList;
    }

    public Integer getUser() { return user; }

    public void setUser(Integer user) { this.user = user; }

    public String getPhoto() { return photo; }

    public void setPhoto(String photo) { this.photo = photo; }

    public Integer getAge() { return age; }

    public void setAge(Integer age) { this.age = age; }

    public String getBio() { return bio; }

    public void setBio(String bio) { this.bio = bio; }

    public String getPhone() { return phone; }

    public void setPhone(String phone) { this.phone = phone; }

    public String getGender() { return gender; }

    public void setGender(String gender) { this.gender = gender; }

    public ArrayList<String> getPreferences() { return Parser.getAsList(preferences); }

    public void setPreferences(ArrayList<String> preferences) { this.preferences = Parser.setToString(preferences); }

    public String getAddress() { return address; }

    public void setAddress(String address) { this.address = address; }

    public String getPostalCode() { return postalCode; }

    public void setPostalCode(String postalCode) { this.postalCode = postalCode; }

    public ArrayList<String> getRsvpList() {return rsvpList;}

}

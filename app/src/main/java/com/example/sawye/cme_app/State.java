package com.example.sawye.cme_app;

import com.example.sawye.cme_app.model.CMe;
import com.example.sawye.cme_app.model.Event;
import com.example.sawye.cme_app.model.Profile;
import com.example.sawye.cme_app.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sawye on 11/16/2017.
 */

public class State {

    // User info
    public static List<CMe<User>> mUser;
    public static List<CMe<Profile>> mProfile;

    // Settings
    public static List<String> mCategories;

    // Event Lists
    public static ArrayList<CMe<Event>> mEventArray;
    public static ArrayList<CMe<Event>> mRsvpEventArray;
    public static ArrayList<CMe<Event>> mHostEventArray;
    public static EventAdapter mEventAdapter;
    public static EventAdapter mRsvpEventAdapter;
    public static EventAdapter mHostEventAdapter;

    // Search settings
    public static boolean sportsCheckboxState;
    public static boolean musicCheckboxState;
    public static boolean videogamesCheckboxState;
    public static boolean movieCheckboxState;
    public static boolean studyCheckboxState;
    public static int radiusBarValue = 50;

    public static double mLongitude;
    public static double mLatitude;

    public static void updateState() {
        mCategories.clear();
        if (sportsCheckboxState) {
            mCategories.add(Event.Category.SPORTS.getCode());
        }
        if (musicCheckboxState) {
            mCategories.add(Event.Category.MUSIC.getCode());
        }
        if (videogamesCheckboxState) {
            mCategories.add(Event.Category.VIDEO_GAMES.getCode());
        }
        if (movieCheckboxState) {
            mCategories.add(Event.Category.MOVIE.getCode());
        }
        if (studyCheckboxState) {
            mCategories.add(Event.Category.STUDY.getCode());
        }
    }
}

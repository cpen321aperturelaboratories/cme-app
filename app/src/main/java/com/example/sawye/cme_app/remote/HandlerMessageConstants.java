package com.example.sawye.cme_app.remote;

/**
 * Created by sawye on 10/15/2017.
 */

public interface HandlerMessageConstants {

    int MESSAGE_LOGIN_RESPONSE          = 0;
    int MESSAGE_LOGOUT_RESPONSE         = 1;
    int MESSAGE_USER_RESPONSE           = 2;
    int MESSAGE_PROFILE_RESPONSE        = 3;
    int MESSAGE_PROFILE_PHOTO_RESPONSE  = 4;
    int MESSAGE_EVENT_RESPONSE          = 5;
    int MESSAGE_EVENT_PHOTO_RESPONSE    = 6;
    int MESSAGE_EVENTS_RESPONSE         = 7;
    int MESSAGE_EVENTS_ID_RESPONSE      = 8;

    int REQUEST_CREATE = 0;
    int REQUEST_DELETE = 1;
    int REQUEST_GET    = 2;
    int REQUEST_UPDATE = 3;
    int REQUEST_JOIN   = 4;
    int REQUEST_LEAVE  = 5;

    int OPERATION_SUCCESS               = 0;
    int OPERATION_FAILURE_UNAUTHORIZED  = 1;
    int OPERATION_FAILURE_FORBIDDEN     = 2;
    int OPERATION_FAILURE_NOT_FOUND     = 3;
    int OPERATION_FAILURE_UNPROCESSABLE = 4;
    int OPERATION_FAILURE_SERVER_ERROR  = 5;


}

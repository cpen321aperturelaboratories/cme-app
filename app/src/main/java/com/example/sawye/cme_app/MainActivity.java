package com.example.sawye.cme_app;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.example.sawye.cme_app.control.FileUtils;
import com.example.sawye.cme_app.model.CMe;
import com.example.sawye.cme_app.model.Event;
import com.example.sawye.cme_app.model.Profile;
import com.example.sawye.cme_app.model.User;
import com.example.sawye.cme_app.remote.HandlerMessageConstants;
import com.example.sawye.cme_app.remote.ServerInterface;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements GenderDialogFragment.GenderDialogListener {


    //----------------------------------------------------------------------------------------------
    // THINGS
    //----------------------------------------------------------------------------------------------
    private static final int PICK_IMAGE = 1;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;
    private static final int MY_PERMISSIONS_ACCESS_FINE_LOCATION = 2;
    private static final int FEED_TAB = 0;
    public static final String FINISH_THIS = "FINISH_THIS";
    public static final String UPDATE_RSVP = "UPDATE_RSVP";

    private static final Integer NUM_EVENTS_BY_OWNER = 100;
    private static final Integer INDEX_BY_OWNER = 0;
    private static final Integer NUM_EVENTS_BY_CATEGORY = 50;
    private static final Integer INDEX_BY_CATEGORY = 0;
    private static final String SORT_BY_DISTANCE = "DISTANCE";
    private static final String SORT_BY_TIME = "TIME";

    public static MainActivity ref;
    private ServerInterface mInterface;
    private Handler mHandler;

    private ListView eventListView;
    private ListView rsvpEventListView;
    private ListView hostEventListView;
    private TextView username;
    private EditText email;
    private EditText bio;
    private EditText phone;
    private EditText age;
    private TextView gender;
    private ImageView profilePicture;
    private TextView alertMessage;

    private TabLayout mTabLayout;
    private ViewFlipper vf;

    private SwipeRefreshLayout feedRefresh;
    private SwipeRefreshLayout eventsRefresh;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.hasExtra(FINISH_THIS)) {
            Intent intent1 = new Intent(this, LoginActivity.class);
            startActivity(intent1);
            finish();
        }
        if (intent.hasExtra(UPDATE_RSVP)) {
            if (eventsRefresh != null)
            refreshEvents();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Ensure always portrait
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        ////////////////
        // Mode Setup //
        ////////////////
        vf = (ViewFlipper) findViewById(R.id.viewFlipper);
        vf.setDisplayedChild(vf.indexOfChild(findViewById(R.id.section_feed)));

        Toolbar myToolbar = (Toolbar) findViewById(R.id.activity_main_toolbar);
        setSupportActionBar(myToolbar);

        ////////////////////
        // Tab Switching //
        ////////////////////
        mTabLayout = (TabLayout) findViewById(R.id.tab_layout_modes);
        mTabLayout.addOnTabSelectedListener(new ModeSelectedListener());

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
        }

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[] { Manifest.permission.ACCESS_FINE_LOCATION },
                    MY_PERMISSIONS_ACCESS_FINE_LOCATION);
        }

        // Handler
        mHandler = new MainActivity.CommunicationHandler();

        // Server interface
        mInterface = ServerInterface.getInstance();
        mInterface.setHandler(mHandler);
        
        // Text fields
        username = (TextView) this.findViewById(R.id.profile_username_field);
        email = (EditText) this.findViewById(R.id.profile_email_field);
        bio = (EditText) this.findViewById(R.id.profile_bio_field);
        phone = (EditText) this.findViewById(R.id.profile_phone_field);
        age = (EditText) this.findViewById(R.id.profile_age_field);
        gender = (TextView) this.findViewById(R.id.profile_gender_field);
        profilePicture = (ImageView) this.findViewById(R.id.profile_picture_field);
        alertMessage = (TextView) this.findViewById(R.id.alert_message);
        bio.setHorizontallyScrolling(false);
        bio.setLines(5);

        age.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // Update profile info
                    Integer newAge = Integer.parseInt(v.getText().toString());
                    State.mProfile.get(0).getFields().setAge(newAge);
                    mInterface.updateProfile(mInterface.profileId, State.mProfile);
                    return false;
                }
                return false;
            }
        });

        phone.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // Update profile info
                    String newPhone = v.getText().toString();
                    State.mProfile.get(0).getFields().setPhone(newPhone);
                    mInterface.updateProfile(mInterface.profileId, State.mProfile);
                    return false;
                }
                return false;
            }
        });

        bio.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    // Update profile info
                    String newBio = v.getText().toString();
                    State.mProfile.get(0).getFields().setBio(newBio);
                    mInterface.updateProfile(mInterface.profileId, State.mProfile);
                    return false;
                }
                return false;
            }
        });

        State.mEventArray = new ArrayList<>();
        State.mRsvpEventArray = new ArrayList<>();
        State.mHostEventArray = new ArrayList<>();

        // Create the adapter to convert the array to views
        State.mEventAdapter = new EventAdapter(this, State.mEventArray);
        State.mRsvpEventAdapter = new EventAdapter(this, State.mRsvpEventArray);
        State.mHostEventAdapter = new EventAdapter(this, State.mHostEventArray);

        State.mCategories = new ArrayList<>();

        // Attach the adapter to a ListView
        eventListView = (ListView) findViewById(R.id.event_list);
        eventListView.setAdapter(State.mEventAdapter);
        rsvpEventListView = (ListView) findViewById(R.id.rsvp_event_list);
        rsvpEventListView.setAdapter(State.mRsvpEventAdapter);
        hostEventListView = (ListView) findViewById(R.id.host_event_list);
        hostEventListView.setAdapter(State.mHostEventAdapter);

        State.mEventAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                ListUtils.setDynamicHeight(eventListView);
            }
        });

        State.mRsvpEventAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                ListUtils.setDynamicHeight(rsvpEventListView);
            }
        });

        State.mHostEventAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                ListUtils.setDynamicHeight(hostEventListView);
            }
        });

        // Fetch saved settings
        SharedPreferences mPref = getSharedPreferences("settings", 0);
        State.sportsCheckboxState = mPref.getBoolean("sportsPref", false);
        State.musicCheckboxState = mPref.getBoolean("musicPref", false);
        State.videogamesCheckboxState = mPref.getBoolean("videogamePref", false);
        State.movieCheckboxState = mPref.getBoolean("moviePref", false);
        State.studyCheckboxState = mPref.getBoolean("studyPref", false);
        State.radiusBarValue = mPref.getInt("radiusPref", 50);

        // Update category list
        State.updateState();

        feedRefresh = (SwipeRefreshLayout) findViewById(R.id.section_feed);
        feedRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshFeed();
            }
        });
        eventsRefresh = (SwipeRefreshLayout) findViewById(R.id.section_events);
        eventsRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshEvents();
            }
        });

        LocationManager mLocationManager = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);

        LocationListener mLocationListener = new myLocationListener();
        mLocationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER, 5000, 10, mLocationListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ref = this;
        mInterface.setHandler(mHandler);

        refreshFeed();
        refreshProfile();
        refreshEvents();
    }

    @Override
    protected void onStop(){
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // Settings option
        if (id == R.id.action_settings) {
            // Go to settings menu
            Intent intent1 = new Intent(this, SettingsActivity.class);
            startActivity(intent1);
            return true;
        }
        // Create event option
        if (id == R.id.action_create_event) {
            // Go to settings menu
            Intent intent1 = new Intent(this, CreateEventActivity.class);
            startActivity(intent1);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void refreshFeed() {
        feedRefresh.setRefreshing(true);
        State.mEventArray.clear();
        String sortStrategy = State.radiusBarValue > 0 ? SORT_BY_DISTANCE : SORT_BY_TIME;
        // TODO: Server is returning an empty event list due to way event_index is parsed in search
        // mInterface.getEventsByCategories(State.mCategories, sortStrategy, INDEX_BY_CATEGORY, NUM_EVENTS_BY_CATEGORY);
        mInterface.getEventsByCategories(State.mCategories, null, INDEX_BY_CATEGORY, NUM_EVENTS_BY_CATEGORY);
    }

    private void refreshEvents() {
        eventsRefresh.setRefreshing(true);
        State.mHostEventArray.clear();
        // TODO: Server is returning an empty event list due to error in get_event_time_remaining
        // mInterface.getEventsByOwner(mInterface.userId, SORT_BY_TIME, INDEX_BY_OWNER, NUM_EVENTS_BY_OWNER);
        mInterface.getEventsByOwner(mInterface.userId, null, INDEX_BY_OWNER, NUM_EVENTS_BY_OWNER);

        if (State.mProfile != null) {
            mInterface.getProfile(mInterface.profileId);
            ArrayList<String> rsvp_list = State.mProfile.get(0).getFields().getRsvpList();
            if (State.mProfile.get(0).getFields().getRsvpList() != null) {
                State.mRsvpEventArray.clear();
                State.mRsvpEventAdapter.notifyDataSetChanged();
                for (String rsvp_event_id : rsvp_list) {
                    mInterface.getEventById(Integer.parseInt(rsvp_event_id));
                }
            }
        }
    }

    private void refreshProfile() {
        // Fetch profile initially
        mInterface.getProfile(mInterface.profileId);
        mInterface.getProfilePhoto(mInterface.profileId);
    }

    //----------------------------------------------------------------------------------------------
    // BUTTON HANDLERS
    //----------------------------------------------------------------------------------------------
    public void buttonPress(View view) {
        switch(view.getId()) {
            case R.id.profile_picture_field:
                Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                getIntent.setType("image/*");
                Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                pickIntent.setType("image/*");
                Intent chooserIntent = Intent.createChooser(getIntent, "Select Image");
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});
                startActivityForResult(Intent.createChooser(chooserIntent, "Select Picture"), PICK_IMAGE);
                break;
            case R.id.profile_gender_field:
                GenderDialogFragment newDialog = new GenderDialogFragment();
                newDialog.show(getSupportFragmentManager(), "gender");
                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                return;
            }
            try {
                InputStream inputStream = getContentResolver().openInputStream(data.getData());
                Bitmap bm = BitmapFactory.decodeStream(inputStream);
                profilePicture.setImageBitmap(bm);

                Uri selectedImage = data.getData();
                String path = FileUtils.getPath(this, selectedImage);
                File file = new File(path);
                mInterface.uploadProfilePhoto(mInterface.profileId, file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }


    //----------------------------------------------------------------------------------------------
    // TAB HANDLER
    //----------------------------------------------------------------------------------------------
    private class ModeSelectedListener implements TabLayout.OnTabSelectedListener {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            Log.i("TAB_POSITION", String.valueOf(tab.getPosition()));

            ViewFlipper vf = (ViewFlipper) findViewById(R.id.viewFlipper);

            switch(tab.getPosition()) {
                case 0:
                    if (eventsRefresh.isRefreshing()) {
                        eventsRefresh.setRefreshing(false);
                    }
                    if (State.mEventArray.size() == 0) {
                        vf.setDisplayedChild(vf.indexOfChild(findViewById(R.id.alert_message)));
                    }
                    else {
                        vf.setDisplayedChild(vf.indexOfChild(findViewById(R.id.section_feed)));
                    }
                    setTitle(getResources().getString(R.string.feed_label));
                    break;
                case 1:
                    if (feedRefresh.isRefreshing()){
                        feedRefresh.setRefreshing(false);
                    }
                    if (eventsRefresh.isRefreshing()) {
                        eventsRefresh.setRefreshing(false);
                    }
                    vf.setDisplayedChild(vf.indexOfChild(findViewById(R.id.section_profile)));
                    setTitle(getResources().getString(R.string.profile_label));
                    mInterface.getUser(mInterface.userId);
                    mInterface.getProfile(mInterface.profileId);
                    mInterface.getProfilePhoto(mInterface.profileId);
                    break;
                case 2:
                    if (feedRefresh.isRefreshing()) {
                        feedRefresh.setRefreshing(false);
                    }
                    vf.setDisplayedChild(vf.indexOfChild(findViewById(R.id.section_events)));
                    setTitle(getResources().getString(R.string.events_label));
                    break;
                default:
                    break;
            }
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {}
        @Override
        public void onTabReselected(TabLayout.Tab tab) {}
    }

    private class CommunicationHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            feedRefresh.setRefreshing(false);
            eventsRefresh.setRefreshing(false);
            switch(msg.what) {
                case HandlerMessageConstants.MESSAGE_USER_RESPONSE:
                    if (msg.arg1 == HandlerMessageConstants.REQUEST_GET) {
                        if (msg.arg2 == HandlerMessageConstants.OPERATION_SUCCESS) {
                            // Set user fields
                            State.mUser = ((List<CMe<User>>) msg.obj);
                            username.setText(State.mUser.get(0).getFields().getUsername());
                            email.setText(State.mUser.get(0).getFields().getEmail());
                        }
                    }
                    break;
                case HandlerMessageConstants.MESSAGE_PROFILE_RESPONSE:
                    if (msg.arg1 == HandlerMessageConstants.REQUEST_GET) {
                        if (msg.arg2 == HandlerMessageConstants.OPERATION_SUCCESS) {
                            // Set profile fields
                            State.mProfile = ((List<CMe<Profile>>) msg.obj);
                            bio.setText(State.mProfile.get(0).getFields().getBio());
                            age.setText(State.mProfile.get(0).getFields().getAge().toString());
                            phone.setText(State.mProfile.get(0).getFields().getPhone());
                            gender.setText(" "+ State.mProfile.get(0).getFields().getGender());
                        }
                    }
                    break;
                case HandlerMessageConstants.MESSAGE_PROFILE_PHOTO_RESPONSE:
                    if (msg.arg1 == HandlerMessageConstants.REQUEST_GET) {
                        if (msg.arg2 == HandlerMessageConstants.OPERATION_SUCCESS) {
                            // Set profile photo
                            InputStream[] bytes = ((InputStream[]) msg.obj);
                            Bitmap bm = BitmapFactory.decodeStream(bytes[0]);
                            profilePicture.setImageBitmap(bm);
                        }
                    }
                    break;
                case HandlerMessageConstants.MESSAGE_EVENT_RESPONSE:
                    if (msg.arg1 == HandlerMessageConstants.REQUEST_GET) {
                        if (msg.arg2 == HandlerMessageConstants.OPERATION_SUCCESS) {
                            List<CMe<Event>> event = ((List<CMe<Event>>) msg.obj);
                            State.mRsvpEventArray.add(event.get(0));
                            State.mRsvpEventAdapter.notifyDataSetChanged();
                            }
                        }
                    break;
                case HandlerMessageConstants.MESSAGE_EVENTS_RESPONSE:
                    if (msg.arg1 == HandlerMessageConstants.REQUEST_GET) {
                        if (msg.arg2 == HandlerMessageConstants.OPERATION_SUCCESS) {
                            List<CMe<Event>> events = ((List<CMe<Event>>) msg.obj);
                            for (CMe<Event> event : events) {
                                State.mEventArray.add(event);
                            }
                            State.mEventAdapter.notifyDataSetChanged();

                            if (mTabLayout.getSelectedTabPosition() == FEED_TAB) {
                                if (State.mEventArray.size() == 0) {
                                    vf.setDisplayedChild(vf.indexOfChild(findViewById(R.id.alert_message)));
                                    alertMessage.setText(getResources().getString(R.string.empty_feed_message));
                                } else {
                                    vf.setDisplayedChild(vf.indexOfChild(findViewById(R.id.section_feed)));
                                }
                            }
                        }
                        else if (msg.arg2 == HandlerMessageConstants.OPERATION_FAILURE_UNPROCESSABLE) {
                            // User's location is unavailable or null; silently retrieve events by TIME
                            mInterface.getEventsByCategories(State.mCategories, SORT_BY_TIME,
                                    INDEX_BY_CATEGORY, NUM_EVENTS_BY_CATEGORY);
                        }
                        else {
                            vf.setDisplayedChild(vf.indexOfChild(findViewById(R.id.alert_message)));
                            alertMessage.setText(getResources().getString(R.string.connection_fail_message));
                        }
                        feedRefresh.setRefreshing(false);
                    }
                    break;
                case HandlerMessageConstants.MESSAGE_EVENTS_ID_RESPONSE:
                    if (msg.arg1 == HandlerMessageConstants.REQUEST_GET) {
                        if (msg.arg2 == HandlerMessageConstants.OPERATION_SUCCESS) {
                            List<CMe<Event>> events = ((List<CMe<Event>>) msg.obj);
                            for (CMe<Event> event : events) {
                                State.mHostEventArray.add(event);
                            }
                            State.mHostEventAdapter.notifyDataSetChanged();
                        }
                        eventsRefresh.setRefreshing(false);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void changeGender(DialogFragment dialog){
        if (gender.getText().equals(" ML")) {
            gender.setText(" FL");
            State.mProfile.get(0).getFields().setGender("FL");
        }
        else if (gender.getText().equals(" FL")) {
            gender.setText(" ML");
            State.mProfile.get(0).getFields().setGender("ML");
        }
        else {
            gender.setText(" ML");
            State.mProfile.get(0).getFields().setGender("ML");
        }
        mInterface.updateProfile(mInterface.profileId, State.mProfile);
    }

    // Check if GPS is enabled
    private Boolean displayGpsStatus() {
        ContentResolver contentResolver = getBaseContext()
                .getContentResolver();
        boolean gpsStatus = Settings.Secure
                .isLocationProviderEnabled(contentResolver,
                        LocationManager.GPS_PROVIDER);
        if (gpsStatus) {
            return true;
        } else {
            return false;
        }
    }

    private class myLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location loc) {
            String longitude = "Longitude: " + loc.getLongitude();
            String latitude = "Latitude: " + loc.getLatitude();
            State.mLongitude = loc.getLongitude();
            State.mLatitude = loc.getLatitude();
        }

        @Override
        public void onProviderDisabled(String provider) {}

        @Override
        public void onProviderEnabled(String provider) {}

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    }

    public static class ListUtils {
        public static void setDynamicHeight(ListView mListView) {
            ListAdapter mListAdapter = mListView.getAdapter();
            if (mListAdapter == null) {
                // When adapter is null
                return;
            }
            int height = 0;
            int desiredWidth = MeasureSpec.makeMeasureSpec(mListView.getWidth(), MeasureSpec.UNSPECIFIED);
            for (int i = 0; i < mListAdapter.getCount(); i++) {
                View listItem = mListAdapter.getView(i, null, mListView);
                listItem.measure(desiredWidth, MeasureSpec.UNSPECIFIED);
                height += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = mListView.getLayoutParams();
            params.height = height + (mListView.getDividerHeight() * (mListAdapter.getCount() - 1));
            mListView.setLayoutParams(params);
            mListView.requestLayout();
        }
    }
}

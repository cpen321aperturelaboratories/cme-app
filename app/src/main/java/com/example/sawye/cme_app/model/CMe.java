package com.example.sawye.cme_app.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by JR on 10/16/2017.
 */

public class CMe<T> implements Serializable {

    @SerializedName("model")
    @Expose
    public String model;

    @SerializedName("pk")
    @Expose
    public String pk;

    @SerializedName("fields")
    @Expose
    public T fields;

    public CMe(String model, String pk, T fields) {
        this.model = model;
        this.pk = pk;
        this.fields = fields;
    }

    public T getFields() { return fields; }

    public void setFields(T fields) { this.fields = fields; }
}

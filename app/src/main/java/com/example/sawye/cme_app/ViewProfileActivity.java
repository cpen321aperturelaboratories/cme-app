package com.example.sawye.cme_app;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sawye.cme_app.model.CMe;
import com.example.sawye.cme_app.model.Event;
import com.example.sawye.cme_app.model.Profile;
import com.example.sawye.cme_app.model.User;
import com.example.sawye.cme_app.remote.HandlerMessageConstants;
import com.example.sawye.cme_app.remote.ServerInterface;

import java.io.InputStream;
import java.util.List;

/**
 * Created by sawye on 11/19/2017.
 */

public class ViewProfileActivity extends AppCompatActivity {

    private TextView username;
    private EditText email;
    private EditText bio;
    private EditText phone;
    private EditText age;
    private TextView gender;
    private ImageView profilePicture;
    private ServerInterface mInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        CMe<Event> event = (CMe<Event>) bundle.getSerializable("Event");

        // Text fields
        username = (TextView) this.findViewById(R.id.profile_username_field);
        email = (EditText) this.findViewById(R.id.profile_email_field);
        email.setClickable(false);
        bio = (EditText) this.findViewById(R.id.profile_bio_field);
        bio.setClickable(false);
        phone = (EditText) this.findViewById(R.id.profile_phone_field);
        phone.setClickable(false);
        age = (EditText) this.findViewById(R.id.profile_age_field);
        age.setClickable(false);
        gender = (TextView) this.findViewById(R.id.profile_gender_field);
        profilePicture = (ImageView) this.findViewById(R.id.profile_picture_field);
        profilePicture.setClickable(false);
        bio.setHorizontallyScrolling(false);
        bio.setLines(5);

        // Handler
        Handler mHandler = new ViewProfileActivity.CommunicationHandler();

        // Server interface
        mInterface = ServerInterface.getInstance();
        mInterface.setHandler(mHandler);

        mInterface.getUser(event.getFields().getOwner());
        mInterface.getUserProfile(event.getFields().getOwner());
    }

    private class CommunicationHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case HandlerMessageConstants.MESSAGE_USER_RESPONSE:
                    if (msg.arg1 == HandlerMessageConstants.REQUEST_GET) {
                        if (msg.arg2 == HandlerMessageConstants.OPERATION_SUCCESS) {
                            // Set user fields
                            List<CMe<User>> hostUser = ((List<CMe<User>>) msg.obj);
                            username.setText(hostUser.get(0).getFields().getUsername());
                            email.setText(hostUser.get(0).getFields().getEmail());
                        }
                    }
                    break;
                case HandlerMessageConstants.MESSAGE_PROFILE_RESPONSE:
                    if (msg.arg1 == HandlerMessageConstants.REQUEST_GET) {
                        if (msg.arg2 == HandlerMessageConstants.OPERATION_SUCCESS) {
                            // Set profile fields
                            List<CMe<Profile>> hostProfile = ((List<CMe<Profile>>) msg.obj);
                            bio.setText(hostProfile.get(0).getFields().getBio());
                            age.setText(hostProfile.get(0).getFields().getAge().toString());
                            phone.setText(hostProfile.get(0).getFields().getPhone());
                            gender.setText(" " + hostProfile.get(0).getFields().getGender());
                        }
                    }
                    else if (msg.arg1 == HandlerMessageConstants.REQUEST_UPDATE) {
                        if (msg.arg2 == HandlerMessageConstants.OPERATION_SUCCESS) {
                            Integer hostProfile = (Integer)msg.obj;
                            mInterface.getProfile(hostProfile);
                            mInterface.getProfilePhoto(hostProfile);
                        }
                    }
                    break;
                case HandlerMessageConstants.MESSAGE_PROFILE_PHOTO_RESPONSE:
                    if (msg.arg1 == HandlerMessageConstants.REQUEST_GET) {
                        if (msg.arg2 == HandlerMessageConstants.OPERATION_SUCCESS) {
                            // Set profile photo
                            InputStream[] bytes = ((InputStream[]) msg.obj);
                            Bitmap bm = BitmapFactory.decodeStream(bytes[0]);
                            profilePicture.setImageBitmap(bm);
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }
}

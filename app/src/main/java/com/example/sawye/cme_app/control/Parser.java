package com.example.sawye.cme_app.control;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by JR on 10/23/2017.
 */

public final class Parser {

    private static final String PATTERN = "[\\[\\]\"\\s]";
    private static final String EMPTY   = "";
    private static final String LEFT    = "[";
    private static final String RIGHT   = "]";
    private static final String QUOTE   = "\"";
    private static final String COMMA   = ",";
    private static final String SPACE   = " ";

    private Parser() {}

    public static ArrayList<String> getAsList(String toBeParsed) {
        String parsed = toBeParsed.replaceAll(PATTERN, EMPTY);
        ArrayList<String> parsedList = new ArrayList<>(Arrays.asList(parsed.split(COMMA)));

        return parsedList;
    }

    public static String setToString(ArrayList<String> toBeParsed) {
        StringBuilder parsed = new StringBuilder(LEFT);
        for (String item : toBeParsed) { parsed.append(QUOTE + item + QUOTE + COMMA + SPACE); }
        parsed.delete(parsed.length() - 2, parsed.length());
        parsed.append(RIGHT);

        return parsed.toString();
    }
}

package com.example.sawye.cme_app;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sawye.cme_app.model.CMe;
import com.example.sawye.cme_app.model.Event;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by sawye on 11/1/2017.
 */

public class EventAdapter extends ArrayAdapter<CMe<Event>> implements Serializable {

    private Context context ;
    public ArrayList<CMe<Event>> events;

    private static EventAdapter mInstance;

    public EventAdapter(Context _context, ArrayList<CMe<Event>>_events) {
        // Call base class constructor
        super(_context, 0, _events);
        context = _context;
        events = _events;
    }

    @Override
    public View getView (int position, View convertView, ViewGroup parent) {
        // Get event at this position
        Event event = getItem(position).getFields();

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.event, parent, false);
        }

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.event, parent, false);

        // Lookup view for data population
        TextView eventName = (TextView) view.findViewById(R.id.event_name);
        TextView eventDistance = (TextView)view.findViewById(R.id.event_distance);
        TextView eventDate = (TextView) view.findViewById(R.id.event_date);
        ImageView eventPhoto = (ImageView) view.findViewById(R.id.event_image);

        // Populate the data into the template view using the data object
        eventName.setText(event.getName());

        Location crntLocation = new Location("crntlocation");
        crntLocation.setLatitude(State.mLatitude);
        crntLocation.setLongitude(State.mLongitude);
        Location newLocation = new Location("newlocation");
        newLocation.setLatitude(event.getLatitude());
        newLocation.setLongitude(event.getLongitude());

        // float distance = crntLocation.distanceTo(newLocation); in meters
        float distance = crntLocation.distanceTo(newLocation); // in meters
        if (distance >= 1000) {
            distance = distance / 1000;
            eventDistance.setText(String.format("%.1f" + " km\n away", distance));
        } else {
            eventDistance.setText(Integer.toString((int) distance)+ "m\n away");
        }

        eventDate.setText(getDate(event.getEventStart()));
        setImage(event, eventPhoto);

        view.setTag(position);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = (Integer) view.getTag();
                // Access the row position here to get the correct data item
                CMe<Event> event = getItem(position);
                Intent intent1 = new Intent(context, ViewEventActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("Event", event);
                intent1.putExtras(bundle);
                context.startActivity(intent1);
            }
        });
        // Return the completed view to render on screen
        return view;
    }

    @Override
    public int getCount() {
        return events.size();
    }

    public String getDate(double date) {
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, d MMM yyyy 'at' H:mm", Locale.getDefault());
        return sdf.format(new Date(Double.valueOf(date).longValue() * 1000));
    }

    private void setImage(Event event, ImageView eventPhoto) {
        switch (event.getCategory()) {
            case "SP":
                eventPhoto.setImageResource(R.drawable.sports);
                break;
            case "MU":
                eventPhoto.setImageResource(R.drawable.music);
                break;
            case "VG":
                eventPhoto.setImageResource(R.drawable.videogames);
                break;
            case "MV":
                eventPhoto.setImageResource(R.drawable.movie);
                break;
            case "ST":
                eventPhoto.setImageResource(R.drawable.study);
                break;
            default:
                eventPhoto.setImageResource(R.drawable.sports);
                break;
        }
    }
}

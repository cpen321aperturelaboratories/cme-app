package com.example.sawye.cme_app.remote;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by JR on 10/14/2017.
 */

public class RetrofitClient {

    private static String baseUrl;

    private static volatile RetrofitClient instance;

    private RetrofitClient() {
        // Prevent form the reflection api
        if (instance != null) {
            throw new ExceptionInInitializerError(
                    "Use getInstance() method to get the single instance of this class.");
        }
    }

    public static RetrofitClient getInstance() {
        // Double check locking pattern
        if (instance == null) {                     // Check for the first time
            synchronized (RetrofitClient.class) {   // Check for the second time
                // If there is no instance available...create new one
                if (instance == null) instance = new RetrofitClient();
            }
        }
        return instance;
    }

    public static void init(String path) { baseUrl = path; }

    public Retrofit createAdapter() {
        OkHttpClient.Builder OkHttpBuilder = new OkHttpClient.Builder();

        OkHttpBuilder.cookieJar(new OurCookieJar());

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(OkHttpBuilder.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }
}
